<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Comision extends Model
{
    protected $table = 'Comisiones';

    protected $fillable = ['nombre','valorMenor','valorMayor','taza'];
}
