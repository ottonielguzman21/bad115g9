<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Canton extends Model
{
    protected $table = 'Cantones';

    protected $fillable = ['nombre'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function direcciones() {
        return $this->hasMany('App\Direccion');
    }
}
