<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class CentroCosto extends Model
{
    protected $table = 'CentroCostos';

    protected $fillable = [
        'numero',
        'monto',
        'periocidad',
        'anioCalendario'
    ];

    /**
     * @return UnidadOrganizacional
     */
    public function unidadOrganizacional()
    {
        return $this->belongsTo('App\UnidadOrganizacional');
    }

}
