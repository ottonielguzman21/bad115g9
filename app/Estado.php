<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'Estados';

    protected $fillable = ['detalle'];

    /**
     * @return Subregion
     */
    public function subregion()
    {
        return $this->belongsTo('App\Subregion');
    }

    /**
     * @return Canton
     */
    public function canton()
    {
        return $this->belongsTo('App\Canton');
    }

    /**
     * @return Empleado
     */
    public function empleado()
    {
        return $this->belongsTo('App\Empleado');
    }
}
