<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Descuento extends Model
{
    protected $table = 'Descuentos';

    protected $fillable = ['nombre','taza','montoFijo','tipo'];

    public function renta()
    {
        return $this->belongsTo('App\Renta');
    }
}
