<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'Regiones';

    protected $fillable = ['nombre'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subregiones() {
        return $this->hasMany('App\Subregion');
    }
       
       //Modifique la funcion para tratar de hacer lo del select din
    public function pais() {
        return $this->belongsTo('App\Pais');
    }
}
