<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Subregion extends Model
{
    protected $table = 'Subregiones';

    protected $fillable = ['nombre'];

    /**
     * @return Region
     */
    public function region() {
        return $this->belongsTo('App\Region');
    }
}
