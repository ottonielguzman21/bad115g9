<?php

namespace bab115g9\Http\Controllers;


use bab115g9\PuestoTrabajo;
use Illuminate\Http\Request;

class PuestosTrabajoController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $usuario = auth()->user();
        return view('puesto.home',['puestos' => PuestoTrabajo::all()
            ->where('empresa','=',$usuario->empresa_id)]);
    }

    public function show(PuestoTrabajo $puesto)
    {
        return view('puesto.show', compact('puesto'));
    }

    public function create()
    {
        return view('puesto.new');
    }

    public function store(Request $request)
    {
        $this->validatePuesto($request,true);
        $puesto = new PuestoTrabajo([
           'nombre' => $request->get('nombre'),
            'descripcion' => $request->get('descripcion'),
            'salarioMaximo' => $request->get('salarioMaximo'),
            'salarioMinimo' => $request->get('salarioMinimo')
        ]);
        $puesto->save();
        return redirect('/puestos')->with('message','Puesto guardado correctamente.');
    }

    public function edit($id)
    {
        $puesto = PuestoTrabajo::find($id);
        return view('puesto.edit',compact('puesto'));
    }

    public function update(Request $request, $id)
    {
        $this->validatePuesto($request, false);
        $puesto = PuestoTrabajo::find($id);
        $puesto->nombre = $request->get('nombre');
        $puesto->descripcion = $request->get('descripcion');
        $puesto->salarioMaximo = $request->get('salarioMaximo');
        $puesto->salarioMinimo = $request->get('salarioMinimo');
        $puesto->save();
        return redirect('/puestos')->with('message','Puesto actualizado correctamente.');
    }

    public function destroy(PuestoTrabajo $puesto)
    {
        $puesto->delete();
        return redirect('/puestos')->with('message','Puesto eliminado correctamente.');
    }

    private function validatePuesto($request, $newObject)
    {
        if($newObject){
            $request->validate(['nombre'=>'unique:PuestosTrabajo']);
        }
        $request->validate([
            'nombre'=>'required|max:50',
            'descripcion'=>'max:250',
            'salarioMaximo'=>'required|regex:/^\d+(\.\d{1,2})?$/',
            'salarioMinimo'=>'required|regex:/^\d+(\.\d{1,2})?$/'
        ]);
    }

}