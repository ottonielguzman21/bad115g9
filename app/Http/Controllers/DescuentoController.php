<?php

namespace bab115g9\Http\Controllers;

use bab115g9\Descuento;
use Illuminate\Http\Request;

class DescuentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $descuentos = Descuento::all();
        return view('descuento.index', compact('descuentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('descuento.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateDescuento($request);
        $request->validate(['nombre'=>'unique:Descuentos']);
        $descuento = new Descuento([
            'nombre' => $request->get('nombre'),
            'taza' => $request->exists('taza')? $request->get('taza'): 0.00,
            'montoFijo' => $request->exists('montoFijo')? $request->get('montoFijo'): 0.00,
            'tipo' => $request->get('tipo')
        ]);
        $descuento->save();
        return redirect('/descuentos')->with('message','Nuevo descuento guardado correctamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $descuento = Descuento::find($id);
        return $descuento;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $descuento = Descuento::find($id);
        return view('descuento.edit', compact('descuento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateDescuento($request);
        $descuento = Descuento::find($id);
        $descuento->nombre = $request->get('nombre');
        $descuento->taza = $request->exists('taza')? $request->get('taza'): 0.00;
        $descuento->montoFijo = $request->exists('montoFijo')? $request->get('montoFijo'): 0.00;
        $descuento->tipo = $request->get('tipo');
        $descuento->save();
        return redirect('/descuentos')->with('message','Descuento actualizado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $descuento = Descuento::find($id);
        $descuento->delete();
        return redirect('/descuentos')->with('message','Descuento eliminado correctamente.');
    }

    public function validateDescuento($request){
        $request->validate([
            'nombre'=>'required|max:50',
            'taza'=>'regex:/^\d+(\.\d{1,2})?$/',
            'montoFijo'=>'regex:/^\d+(\.\d{1,2})?$/',
            'tipo'=>'required'
        ]);
    }
}
