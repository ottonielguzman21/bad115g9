<?php

namespace bab115g9\Http\Controllers;

use Illuminate\Http\Request;
use bab115g9\Http\Requests;
use bab115g9\Genero;
//use App\Genero;

class GeneroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     
       if ($request){
            
            $gen = Genero::orderBy('id','DESC')->paginate(5);
            return view('genero.index', compact('gen'));
         
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('genero.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


       $request->validate(['tipo'=>'required']);
           
        
        $genero = new Genero(['tipo'=>$request->get('tipo')]);
        $genero->save();

        return redirect('/genero')->with('message','Nuevo Genero guardado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $generos=genero::find($id);
      
       return  view('genero.show', compact('generos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $genero=Genero::find($id);
        return view('genero.edit',compact('genero'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[ 
                
                'tipo'=>'required']);
 
        genero::find($id)->update($request->all());
        return redirect()->route('genero.index')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Genero::find($id)->delete();
        return redirect()->route('genero.index')->with('success','Registro eliminado satisfactoriamente');
    }
}
