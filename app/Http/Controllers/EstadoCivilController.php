<?php

namespace bab115g9\Http\Controllers;

use Illuminate\Http\Request;
use bab115g9\EstadoCivil;

class EstadoCivilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request){
            
            $est = EstadoCivil::orderBy('id','DESC')->paginate(5);
            return view('estadocivil.index', compact('est'));
         
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('estadocivil.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate(['tipo'=>'required']);
           
        
        $estadoc = new EstadoCivil(['tipo'=>$request->get('tipo')]);
        $estadoc->save();

        return redirect('/estadocivil')->with('message','Nuevo Estado Civil guardado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $est=EstadoCivil::find($id);
        return view('estadocivil.edit',compact('est'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[ 
                
                'tipo'=>'required']);
 
        estadoCivil::find($id)->update($request->all());
        return redirect()->route('estadocivil.index')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         EstadoCivil::find($id)->delete();
        return redirect()->route('estadocivil.index')->with('success','Registro eliminado satisfactoriamente');
    



    }
}
