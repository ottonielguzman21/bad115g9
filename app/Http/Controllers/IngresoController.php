<?php

namespace bab115g9\Http\Controllers;

use bab115g9\Ingreso;
use Illuminate\Http\Request;

class IngresoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ingresos = Ingreso::all();
        return view('ingreso.index', compact('ingresos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ingreso.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateIngreso($request);
        $request->validate(['nombre'=>'unique:Ingresos']);
        $ingreso = new Ingreso([
           'nombre' => $request->get('nombre'),
           'taza' => $request->exists('taza')? $request->get('taza'): 0.00,
            'tipo' => $request->get('tipo'),
            'montoFijo' => $request->exists('montoFijo')? $request->get('montoFijo'): 0.00
        ]);
        $ingreso->save();
        return redirect('/ingresos')->with('message','Ingreso guardado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ingreso = Ingreso::find($id);
        return $ingreso;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ingreso = Ingreso::find($id);
        return view('ingreso.edit', compact('ingreso'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateIngreso($request);
        $ingreso = Ingreso::find($id);
        $ingreso->nombre = $request->get('nombre');
        $ingreso->taza = $request->exists('taza')? $request->get('taza'): 0.00;
        $ingreso->tipo = $request->get('tipo');
        $ingreso->montoFijo = $request->get('montoFijo')? $request->get('montoFijo'): 0.00;
        $ingreso->save();
        return redirect('/ingresos')->with('message', 'Ingreso actualizado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ingreso = Ingreso::find($id);
        $ingreso->delete();
        return redirect('/ingresos')->with('message', 'Ingreso eliminado correctamente.');
    }

    public function validateIngreso($request)
    {
        $request->validate([
           'nombre'=>'required|max:50',
           'taza'=>'regex:/^\d+(\.\d{1,2})?$/',
            'tipo'=>'required',
            'montoFijo'=>'regex:/^\d+(\.\d{1,2})?$/'
        ]);
    }
}
