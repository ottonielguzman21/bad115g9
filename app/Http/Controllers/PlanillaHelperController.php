<?php

namespace bab115g9\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PlanillaHelperController extends Controller
{

    public function getDepartamentos()
    {
        $user = auth()->user();
        return DB::select('select * from getdepartamentos(?)', array($user->empresa_id));
    }

    public function getSubUnidades(Request $request)
    {
        $user = auth()->user();
        return DB::select('select * from getsubunidades(?,?)', array($user->empresa_id,$request->get('padre')));
    }

    public function getUnidadEmpleados(Request $request)
    {
        return DB::select('select * from getempleados(?)', array($request->get('unidad')));
    }

    public function getPlanillaIngresos(Request $request)
    {
        return DB::select('select * from getplanillaingresos(?)', array($request->get('planilla')));
    }

    public function getPlanillaDescuentos(Request $request)
    {
        return DB::select('select * from getplanilladescuentos(?)', array($request->get('planilla')));
    }

    public function getPlanillaComisiones(Request $request)
    {
        return DB::select('select * from getplanillacomisiones(?)', array($request->get('planilla')));
    }

}