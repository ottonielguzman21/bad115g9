<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $table = 'Empleados';

    protected $fillable = ['codigo', 'primerNombre', 'segundoNombre', 'apellidoPaterno', 'apellidoMaterno', 'apellidoCasada',
        'fechaNacimiento', 'dui', 'nit', 'isss', 'nup', 'pasaporte', 'emailPersonal', 'emailInstitucional', 'genero_id', 'estadoCivil_id', 'profesion_id', 'jefe_id', 'estado_id'];

    /**
     * @return Genero
     */
    public function genero()
    {
        return $this->belongsTo('App\Genero');
    }

    /**
     * @return EstadoCivil
     */
    public function estadoCivil()
    {
        return $this->belongsTo('App\EstadoCivil');
    }

    /**
     * @return Profesion
     */
    public function profesion()
    {
        return $this->belongsTo('App\Profesion');
    }

    /**
     * @return Estado
     */
    public function estado()
    {
        return $this->hasMany('App\Estado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subAlternos()
    {
        return $this->hasMany('App\Empleado');
    }

    /**
     * @return Empleado
     */
    public function jefe()
    {
        return $this->belongsTo('App\Empleado');
    }

}
