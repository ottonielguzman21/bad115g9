<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class EstadoCivil extends Model
{
    protected $table = 'EstadoCiviles';
    protected $fillable = ['tipo'];

    public function empleados(){
        return $this->hasMany(Empleado::class);
    }
}
