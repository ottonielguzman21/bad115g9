<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Bloqueo extends Model
{
    protected $table = 'Bloqueos';

    /**
     * @return Usuario
     */
    public function usuario()
    {
        return $this->belongsTo('App\Usuario');
    }

}
