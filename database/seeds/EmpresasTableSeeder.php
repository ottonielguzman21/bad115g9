<?php

use Illuminate\Database\Seeder;

class EmpresasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        //
        DB:: table('Empresas')->insert(array(
            'nombre' =>'Industrias S A y C V',
            'direccion'=>'admin@gmail.com',
            'representateLegal'=>'nayid bukele',
            'nit'=>'0816-300500-103-1',
            'nic'=>'NIC-34',
            'telefono'=>'22357000',
            'email'=>'industrias@gmail.com',
            'web'=>'www.industrias.com',
            'logoDir'=>'1',
        	));

    }
}
