<?php

use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Semillas para las regiones

        // Para El Salvador

        DB::table('Regiones')->insert([
            'nombre' => 'San Salvador',
            'pais_id' => '1',
        ]);

        \DB::table('Regiones')->insert([
            'nombre' => 'San Vicente',
            'pais_id' => '1',
        ]);

        \DB::table('Regiones')->insert([
            'nombre' => 'La Libertad',
            'pais_id' => '1',
        ]);

        // Para Los Estados Unidos

        \DB::table('Regiones')->insert([
            'nombre' => 'California',
            'pais_id' => '2',
        ]);

        \DB::table('Regiones')->insert([
            'nombre' => 'Texas',
            'pais_id' => '2',
        ]);

        \DB::table('Regiones')->insert([
            'nombre' => 'Washington',
            'pais_id' => '2',
        ]);

        //para Colombia

        \DB::table('Regiones')->insert([
            'nombre' => 'Antioquia',
            'pais_id' => '3',
        ]);

        \DB::table('Regiones')->insert([
            'nombre' => 'Cordoba',
            'pais_id' => '3',
        ]);

        \DB::table('Regiones')->insert([
            'nombre' => 'Bogota',
            'pais_id' => '3',
        ]);
    }
}
