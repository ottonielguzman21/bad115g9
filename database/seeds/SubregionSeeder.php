<?php

use Illuminate\Database\Seeder;

class SubregionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//El Salvador
        //San Salvador
    
        DB::table('Subregiones')->insert([
            'nombre' => 'Mejicanos',
            'region_id' => '1',
        ]);

        //San Vicente

        \DB::table('Subregiones')->insert([
            'nombre' => 'Apastepeque',
            'region_id' => '2',
        ]);

        //La Libertad

        \DB::table('Subregiones')->insert([
            'nombre' => 'Ciudad Arce',
            'region_id' => '3',
        ]);

        // Para Los Estados Unidos

        // California

        \DB::table('Subregiones')->insert([
            'nombre' => 'Los Angeles',
            'region_id' => '4',
        ]);

        //Texas

        \DB::table('Subregiones')->insert([
            'nombre' => 'Houston',
            'region_id' => '5',
        ]);

        //Washington

        \DB::table('Subregiones')->insert([
            'nombre' => 'Condado de Lincoln',
            'region_id' => '6',
        ]);

        //para Colombia

        //Antioquia

        \DB::table('Subregiones')->insert([
            'nombre' => 'Medellin',
            'region_id' => '7',
        ]);

        //Cordoba

        \DB::table('Subregiones')->insert([
            'nombre' => 'Puerto Escondido',
            'region_id' => '8',
        ]);

        //Bogotá
         // Poner Cundinamarca---
        \DB::table('Subregiones')->insert([
            'nombre' => 'Bogota',
            'region_id' => '9',
        ]);
    }
}
