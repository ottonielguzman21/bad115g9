<?php

use Illuminate\Database\Seeder;

class GeneroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         DB::table('Generos')->insert([
            'tipo' => 'Masculino',
        ]);

        \DB::table('Generos')->insert([
            'tipo' => 'Femenino',
        ]);
    }
}
