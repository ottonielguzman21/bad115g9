<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB:: table('users')->insert(array(
            'name' =>'Administrador del Sistema',
            'email'=>'admin@gmail.com',
            'password'=>\Hash::make('admin1234'),
            'rol_id'=>'1',
            'empresa_id'=>'1',
            'habilitado'=>true,
        	));

        \DB:: table('users')->insert(array(
            'name' =>'Gerente General',
            'email'=>'gerentegeneral@gmail.com',
            'password'=>\Hash::make('gerentegeneral1234'),
            'rol_id'=>'2',
            'empresa_id'=>'1',
            'habilitado'=>true,
        	));

        \DB:: table('users')->insert(array(
            'name' =>'Gerente de Recursos Humanos',
            'email'=>'gerenterrhh@gmail.com',
            'password'=>\Hash::make('gerenterrhh1234'),
            'rol_id'=>'3',
            'empresa_id'=>'1',
            'habilitado'=>true,
        	));

        \DB:: table('users')->insert(array(
            'name' =>'Jefe UO',
            'email'=>'jefeuo@gmail.com',
            'password'=>\Hash::make('jefeuo1234'),
            'rol_id'=>'4',
            'empresa_id'=>'1',
            'habilitado'=>false,
            ));
    }
}
