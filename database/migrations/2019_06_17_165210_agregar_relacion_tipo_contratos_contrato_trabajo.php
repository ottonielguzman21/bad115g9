<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionTipoContratosContratoTrabajo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ContratosTrabajo', function (Blueprint $table) {
            $table->unsignedInteger('tipo_contrato_id');

            $table->foreign('tipo_contrato_id')
                ->references('id')
                ->on('TipoContratos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ContratosTrabajo', function (Blueprint $table) {
            $table->dropColumn('tipo_contrato_id');
        });
    }
}
