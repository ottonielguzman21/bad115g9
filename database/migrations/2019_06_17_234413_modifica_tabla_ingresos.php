<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModificaTablaIngresos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Ingresos', function (Blueprint $table) {
            $table->double('montoFijo',8,2)->nullable(true);
            $table->enum('tipo',['FIJO','BASE'])->default('BASE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Ingresos', function (Blueprint $table) {
            $table->dropColumn('montoFijo');
            $table->dropColumn('tipo');
        });
    }
}
