<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionEstadoSubregion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Estados', function (Blueprint $table) {
            $table->unsignedInteger('subregion_id')->nullable();

            $table->foreign('subregion_id')
                ->references('id')
                ->on('Subregiones')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Estados', function (Blueprint $table) {
            $table->dropColumn('subregion_id');
        });
    }
}
