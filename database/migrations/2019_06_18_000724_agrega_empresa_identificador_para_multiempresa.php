<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregaEmpresaIdentificadorParaMultiempresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Descuentos', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('Ingresos', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('Comisiones', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('PuestosTrabajo', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('ContratosTrabajo', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('CentroCostos', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('Empleados', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('Rentas', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('TipoContratos', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('Generos', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('EstadoCiviles', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('Profesiones', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Descuentos', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('Ingresos', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('Comisiones', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('PuestosTrabajo', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('ContratosTrabajo', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('CentroCostos', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('Empleados', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('Rentas', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('TipoContratos', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('Generos', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('EstadoCiviles', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('Profesiones', function (Blueprint $table) {$table->dropColumn('empresa');});
    }
}
