<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionRegionPais extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Regiones', function (Blueprint $table) {
            $table->unsignedInteger('pais_id');

            $table->foreign('pais_id')
                ->references('id')
                ->on('Paises')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('Regiones', function (Blueprint $table) {
           $table->dropColumn('pais_id');
        });
    }
}
