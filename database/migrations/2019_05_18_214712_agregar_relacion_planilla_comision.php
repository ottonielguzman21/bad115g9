<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionPlanillaComision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Ventas', function (Blueprint $table) {
            $table->unsignedInteger('planilla_id');
            $table->unsignedInteger('comision_id');
            $table->double('monto',8,2);
            $table->foreign('planilla_id')
                ->references('id')
                ->on('Planillas')
                ->onDelete('cascade');
            $table->foreign('comision_id')
                ->references('id')
                ->on('Comisiones')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Ventas');
    }
}
