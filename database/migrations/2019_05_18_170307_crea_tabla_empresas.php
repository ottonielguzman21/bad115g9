<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaEmpresas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',100);
            $table->string('direccion',500);
            $table->string('representateLegal',150)->nullable();
            $table->string('nit',17)->unique();
            $table->string('nic',10)->unique();
            $table->string('telefono',8)->nullable();
            $table->string('email',150)->nullable();
            $table->string('web',100)->nullable();
            $table->string('logoDir',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Empresas');
    }
}
