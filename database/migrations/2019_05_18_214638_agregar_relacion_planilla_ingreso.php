<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionPlanillaIngreso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('IngresosPlanilla', function (Blueprint $table) {
            $table->unsignedInteger('planilla_id');
            $table->unsignedInteger('ingreso_id');
            $table->double('monto',8,2);
            $table->foreign('planilla_id')
                ->references('id')
                ->on('Planillas')
                ->onDelete('cascade');
            $table->foreign('ingreso_id')
                ->references('id')
                ->on('Ingresos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('IngresosPlanilla');
    }
}
