<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaRentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Rentas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tramo');
            $table->double('desde',8,2);
            $table->double('hasta',8,2);
            $table->double('porcentaje',8,2);
            $table->double('sobreExceso',8,2);
            $table->double('cuotaFija',8,2);
            $table->boolean('retencion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Rentas');
    }
}
