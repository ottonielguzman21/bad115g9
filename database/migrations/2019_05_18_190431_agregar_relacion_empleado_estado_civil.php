<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionEmpleadoEstadoCivil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Empleados', function (Blueprint $table) {
            $table->unsignedInteger('estadoCivil_id')->nullable();
            $table->foreign('estadoCivil_id')
                ->references('id')
                ->on('EstadoCiviles')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Empleados', function (Blueprint $table) {
            $table->dropColumn('estadoCivil_id');
        });
    }
}
