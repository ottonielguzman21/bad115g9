<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregaEmpresaIdentificadorTablasRestantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Planillas', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('Paises', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('Regiones', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('Subregiones', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
        Schema::table('Estados', function (Blueprint $table) {$table->integer('empresa')->nullable(true);});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Planillas', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('Paises', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('Regiones', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('Subregiones', function (Blueprint $table) {$table->dropColumn('empresa');});
        Schema::table('Estados', function (Blueprint $table) {$table->dropColumn('empresa');});
    }
}
