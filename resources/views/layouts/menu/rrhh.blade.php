<li>
    <a href="{{ url('empleado.index') }}">
        <i class="material-icons">group</i>
        <span>Empleados</span>
    </a>
</li>
<li>
    <a href="{{ url('genero.index') }}">
        <i class="material-icons">accessibility</i>
        <span>Generos</span>
    </a>
</li>
<li>
    <a href="{{ url('estadocivil.index') }}">
        <i class="material-icons">wc</i>
        <span>Estados civiles</span>
    </a>
</li>
<li>
    <a href="{{ url('profesion.index') }}">
        <i class="material-icons">business_center</i>
        <span>Profesiones u oficios</span>
    </a>
</li>
