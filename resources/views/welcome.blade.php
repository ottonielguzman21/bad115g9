@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="active">
            <i class="material-icons">home</i> Inicio
        </li>
    </ol>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-blue-grey">
                    <h2>
                        Bienvenido {{Auth::user()->name}}
                    </h2>
                </div>
                <div class="body">
                    @if(empty($info))
                        <div class="alert alert-danger">
                            <i class="material-icons">error_outline</i> <strong>Un momento!</strong> Parece que no se ha configurado la información de la empresa.
                        </div>
                        @else
                        <div class="media">
                            <div class="media-left">
                                <a href="javascript:void(0);">
                                    <img class="media-object" src="/images/{{$info->logoDir}}" width="64" height="64">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">{{$info->nombre}}</h4>
                                <p class="m-b-0">{{$info->direccion}}</p>
                                <p class="m-b-0">{{$info->telefono}}</p>
                                <p class="m-b-0"><a href="javascript:">{{$info->email}}</a></p>
                                <p class="m-b-0"><a href="javascript:">{{$info->web}}</a></p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-blue-grey">
                    <h5>Mi Organización</h5>
                    <small>Resumen de la organización</small>
                </div>
                <div class="body">
                    <ul class="list-group">
                        @foreach($departamentos as $departamento)
                            <li class="list-group-item">{{$departamento->nombre}} <span class="badge bg-cyan">{{$departamento->sub}}</span></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection