@extends('layouts.appForm')

@section('content')
<div class="block-header">
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
        <li class="active"><i class="material-icons">content_paste</i> Jerarquia de Empleados</li>
    </ol>
</div>
@if ($errors->any())
<div class="alert alert-danger">
    <h4><i class="material-icons">error</i> Se encontraron los siguientes errores</h4>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Empleados de la empresa {{ $nombre->nombre }}
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>apellidos</th>
                            <th>dui</th>
                            <th>Genero</th>
                            <th>profesion</th>
                            <th>Jefe o subalterno</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($chiefs as $e)
                            <tr>
                                <td>{{ $e->id }}</td>
                                <td>{{ $e->primernombre }} {{ $e->segundonombre }}</td>
                                <td>{{ $e->apellidopaterno }} {{ $e->apellidomaterno }}</td>
                                <td>{{  $e->dui }}</td>

                                @if($e->genero_id==1)
                                <td> Masculino </td>
                                @else
                                <td> Femenino </td>
                                @endif
                                @foreach($profesiones as $pro)
                                    @if($e->profesion_id == $pro->id)
                                        <td>{{ $pro->nombre }}</td>
                                    @endif
                                @endforeach
                                @foreach($chiefs as $em)
                                    @if($e->jefe_id == $em->id)
                                        <td>Sub alterno de: {{ $em->primernombre }} {{$em->apellidopaterno }}</td>
                                    @endif
                                @endforeach
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection