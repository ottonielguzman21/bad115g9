@extends('layouts.applogin')

@section('content')
<div class="card">
  <div class="body">
      {!!Form::open(array('url'=>'usuarios','id'=>'sign_up','class'=>'form-horizontal','role'=>'form','method'=>'POST','autocomplete'=>'off'))!!}
      {{Form::token()}}
      <div class="msg">Registrar Usuario</div>
      <div class="input-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <span class="input-group-addon">
              <i class="material-icons">person</i>
        </span>

        <div class="form-line">
              <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Nombre">

              @if ($errors->has('name'))
              <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
    </div>
</div>
<div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
      <span class="input-group-addon">
        <i class="material-icons">email</i>
  </span>
  <div class="form-line">
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="ejemplo@ejemplo.com">

        @if ($errors->has('email'))
        <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
    </span>
    @endif
</div>

</div>
<div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
      <span class="input-group-addon">
        <i class="material-icons">lock</i>
  </span>
  <div class="form-line">
        <input id="password" type="password" minlength="10" class="form-control" name="password" required placeholder="Contraseña">

        @if ($errors->has('password'))
        <span class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
    </span>
    @endif
</div>
</div>
<div class="input-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
      <span class="input-group-addon">
        <i class="material-icons">lock</i>
  </span>
  <div class="form-line">
        <input id="password-confirm" type="password" minlength="10"  class="form-control" name="password_confirmation" required placeholder="Confirmar Contraseña">
        @if ($errors->has('password_confirmation'))
        <span class="help-block">
           <strong>{{ $errors->first('password_confirmation') }}</strong>
     </span>
     @endif
</div>
</div>

<div class="input-group{{ $errors->has('rol_id') ? ' has-error' : '' }}">
    <span class="input-group-addon">
        <i class="material-icons">list</i>
  </span>
  <div class="form-line">
    <div>                   
        <select name="rol_id" required class="form-control" class="form-control selectpicker"  id="rol_id" value="{{old('rol_id')}}" placeholder="Tipo de Usuario">
            <option value="0">
                 Seleccione el Tipo de Usuario</option>

           @foreach ($roles as $r)
           <option value="{{$r->id}}">
                 {{$r->nombre}}</option>
                 @endforeach
           </select>
     </div>
     @if ($errors->has('rol_id'))
     <span class="help-block">
      <strong>{{ $errors->first('rol_id') }}</strong>
</span>
@endif
</div>
</div>
<div class="input-group{{ $errors->has('empresa_id') ? ' has-error' : '' }}">
    <span class="input-group-addon">
        <i class="material-icons">list</i>
  </span>
  <div class="form-line">                    
        <select name="empresa_id" required class="form-control" class="form-control selectpicker"  id="empresa_id" value="{{old('empresa_id')}}">
            <option value="0">
                 Seleccione la Empresa</option>

           @foreach ($empresas as $e)
           <option value="{{$e->id}}" >
                 {{$e->nombre}}</option>
                 @endforeach
           </select>
           @if ($errors->has('empresa_id'))
           <span class="help-block">
            <strong>{{ $errors->first('empresa_id') }}</strong>
      </span>
      @endif
</div>
</div>



<div class="form-group">
    <input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink">
    <label for="terms">Aceptas<a href="javascript:void(0);">los terminos y condiciones</a>.</label>
</div>

<button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">registrar</button>

<div class="m-t-25 m-b--5 align-center">
    ya tienes cuenta? <a href="{{ route('login') }}">Ingresa con tu cuenta</a>
</div>
<div class="form-group"> 
      <span>
        @include('mensajes.messages')
  </span>
</div>  
{!!Form::close()!!}  
</div>
</div>


@endsection
