@extends('layouts.appTable')
@section('content')
<!--Contenido -->
<div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li class="active"><i class="material-icons">archive</i>Usuarios</li>
        </ol>
</div>


<!-- Basic Examples -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>
          Listado de usuarios
        </h2>
        @include('mensajes.messages')
        <ul class="header-dropdown m-r--5">
          <li class="dropdown">
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="material-icons">more_vert</i>
            </a>
            <ul class="dropdown-menu pull-right">
              <li><a href=" {{ route('usuarios.create') }}" >Nuevo Usuario</a></li>
              <li><a href="{{url('bitacora')}}">Bitacora</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="body">
        <div class="table-responsive">
          <table id="dataTable" class="table table-bordered table-striped table-hover js-basic-example dataTable">
            <thead>
              <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>email</th>
                <th>Tipo</th>
                <th>Descripcion</th>
                <th>Ultima Sesion</th>
                <th>Estado</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
             @foreach ($users as $u)
             <tr>
               <td>{{ $u->id}}</td>
               <td>{{ $u->name }}</td>
               <td>{{ $u->email }}</td>
               <td>{{ $u->nombre }}</td>            
               <td>{{ $u->descripcion }}</td>
               <td>{{ $u->ultimo_login }}</td>
               @if($u->habilitado==1)
               <td>
                <i class="material-icons">check</i></td>
                @else
                <td>
                <i class="material-icons">close</i></td>
                @endif
                <td>
                <ul class="list-unstyled">
                  <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                      <i class="material-icons">more_vert</i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                      <li>
                        <a href="" data-target="#modal-delete-{{$u->id}}" data-toggle="modal"><i class="material-icons">autorenew</i>Cambiar Estado</a></li>
                      <li><a href="{{URL::action('HomeController@edit',$u->id)}}"><i class="material-icons">edit</i> Editar</a></li>
                    </ul>
                  </li>
                </ul>

              </td>
            </tr>
            @include('usuarios.modal')
            @endforeach 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<!-- #END# Basic Examples -->


@endsection