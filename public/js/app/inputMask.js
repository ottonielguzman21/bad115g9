//Masked Input ============================================================================================================================
var $demoMaskedInput = $('.masked-input');

//Date
$demoMaskedInput.find('.date').inputmask('dd/mm/yyyy', { placeholder: '__/__/____' });

//Time
$demoMaskedInput.find('.time12').inputmask('hh:mm t', { placeholder: '__:__ _m', alias: 'time12', hourFormat: '12' });
$demoMaskedInput.find('.time24').inputmask('hh:mm', { placeholder: '__:__ _m', alias: 'time24', hourFormat: '24' });

//Date Time
$demoMaskedInput.find('.datetime').inputmask('d/m/y h:s', { placeholder: '__/__/____ __:__', alias: "datetime", hourFormat: '24' });

//Mobile Phone Number
$demoMaskedInput.find('.mobile-phone-number').inputmask('+99 (999) 999-99-99', { placeholder: '+__ (___) ___-__-__' });
//Phone Number
$demoMaskedInput.find('.phone-number').inputmask('+99 (999) 999-99-99', { placeholder: '+__ (___) ___-__-__' });

//Dollar Money
$demoMaskedInput.find('.money-dollar').inputmask('9{1,6}.9{1,2}', { placeholder: '______.__ $' });
//Euro Money
$demoMaskedInput.find('.money-euro').inputmask('9{1,6}.9{1,2}', { placeholder: '_____.__ €' });

//IP Address
$demoMaskedInput.find('.ip').inputmask('999.999.999.999', { placeholder: '___.___.___.___' });

//Credit Card
$demoMaskedInput.find('.credit-card').inputmask('9999 9999 9999 9999', { placeholder: '____ ____ ____ ____' });

//Email
$demoMaskedInput.find('.email').inputmask({ alias: "email" });

//Serial Key
$demoMaskedInput.find('.key').inputmask('****-****-****-****', { placeholder: '____-____-____-____' });

//Porcent
$demoMaskedInput.find('.porcent').inputmask('9{1,3}.9{1,2}', {placeholder:'___.__ %'});

//NIT
$demoMaskedInput.find('.nit').inputmask('9{4}-9{6}-9{3}-9{1}', {placeholder:'____-______-___-_'});

//Telefono
$demoMaskedInput.find('.tel').inputmask('9{8}', {placeholder:'________'});

$demoMaskedInput.find('.dui').inputmask('9{8}-9{1}', {placeholder:'________-_'});

$demoMaskedInput.find('.isss').inputmask('9{10}', {placeholder:'__________'});

$demoMaskedInput.find('.nup').inputmask('9-a{10,12}', {placeholder:'____________'});

//===========================================================================================================================================